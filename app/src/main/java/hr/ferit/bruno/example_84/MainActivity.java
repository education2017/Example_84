package hr.ferit.bruno.example_84;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    @BindView(R.id.rvBookList) RecyclerView rvBooksList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.setUpUi();
    }

    private void setUpUi() {
        ArrayList<Book> booksList = loadBooks();
        BookAdapter adapter = new BookAdapter(booksList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemDecoration decoration = 
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        this.rvBooksList.addItemDecoration(decoration);
        this.rvBooksList.setLayoutManager(layoutManager);
        this.rvBooksList.setAdapter(adapter);
    }

    private ArrayList<Book> loadBooks() {
        ArrayList<Book> books = new ArrayList<>();

        String[] authors = getResources().getStringArray(R.array.authors);
        String[] titles = getResources().getStringArray(R.array.titles);
        int[] years = getResources().getIntArray(R.array.years);
        String[] urls = getResources().getStringArray(R.array.urls);

        for (int i=0; i<authors.length; i++){
            books.add(new Book(authors[i], titles[i], years[i], urls[i]));
        }

        return books;
    }


}
