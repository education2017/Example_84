package hr.ferit.bruno.example_84;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zoric on 29.8.2017..
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    ArrayList<Book> mBooks;

    public BookAdapter(ArrayList<Book> books) { mBooks = books; }

    @Override
    public BookAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View bookView = inflater.inflate(R.layout.list_item_book, parent, false);
        ViewHolder bookViewHolder = new ViewHolder(bookView);
        return bookViewHolder;
    }

    @Override
    public void onBindViewHolder(BookAdapter.ViewHolder holder, int position) {
        Book book = this.mBooks.get(position);

        Log.d("TAG", holder.tvBookAuthor.getText().toString());

        holder.tvBookTitle.setText(book.getTitle());
        holder.tvBookAuthor.setText(book.getAuthor());
        holder.tvBookYear.setText(String.valueOf(book.getYear()));

        Picasso.with(holder.tvBookTitle.getContext())
                .load(book.getCoverUrl())
                .fit()
                .centerCrop()
                .into(holder.ivBookCover);
    }

    @Override
    public int getItemCount() {
        return this.mBooks.size();
    }

    public static  class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvBookTitle) TextView tvBookTitle;
        @BindView(R.id.tvBookAuthor) TextView tvBookAuthor;
        @BindView(R.id.tvBookYear) TextView tvBookYear;
        @BindView(R.id.ivBookCover) ImageView ivBookCover;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
